function displayPropertiesName(object){
    if (object){
        const result=[]
        for(key in object){
            result.push(key);
        }
        return result;
    }else{
        return []
    }

}

//console.log(displayPropertiesName({ name: 'Bruce Wayne', age: 36, location: 'Gotham' }));

module.exports = displayPropertiesName;