function mapObjectValue(object) {
    if (object) {
        const temp = [object];
        return temp.map(cb);
    } else {
        return {};
    }
    function cb(obj) {
        for (property in obj) {
            obj[property] += 5;
        }
        return obj;
    }
}
console.log(mapObjectValue({ start: 5, End: 6 }));

module.exports = mapObjectValue;