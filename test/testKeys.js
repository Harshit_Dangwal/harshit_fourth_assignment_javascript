const displayPropertiesName = require("../keys");


console.log(displayPropertiesName({ name: 'Bruce Wayne', age: 36, location: 'Gotham' }));
// [ 'name', 'age', 'location' ]
console.log(displayPropertiesName(undefined));
// []
console.log(displayPropertiesName(null));
// []
