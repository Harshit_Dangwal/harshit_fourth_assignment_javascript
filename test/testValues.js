const displayObjectValue = require("../values");

console.log(displayObjectValue({ name: 'Bruce Wayne', age: 36, location: 'Gotham' }));
// [ 'Bruce Wayne', 36, 'Gotham' ]

console.log(displayObjectValue(undefined));
// []