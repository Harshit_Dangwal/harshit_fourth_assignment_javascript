const fillUndefinedProperties = require("../defaults");

console.log(fillUndefinedProperties({ name: 'Bruce Wayne', age: 36, location: 'Gotham' }, { location: 'New York' }))
// { name: 'Bruce Wayne', age: 36, location: 'New York' }

console.log(fillUndefinedProperties({ name: 'Bruce Wayne', age: 36, location: 'Gotham' }, { name: 'Harry' }))
// { name: 'Harry', age: 36, location: 'Gotham' }