const getKeyValuePairs = require("../pairs");

console.log(getKeyValuePairs({ name: 'Bruce Wayne', age: 36, location: 'Gotham' }));
// [ [ 'name', 'Bruce Wayne' ], [ 'age', 36 ], [ 'location', 'Gotham' ] ]

console.log(getKeyValuePairs(undefined));
// []

