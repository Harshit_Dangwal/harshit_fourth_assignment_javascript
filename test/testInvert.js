const getObjectCopy = require("../invert");

console.log(getObjectCopy({ name: 'Bruce Wayne', age: 36, location: 'Gotham' }));
// { '36': 'age', 'Bruce Wayne': 'name', Gotham: 'location' }

console.log(getObjectCopy(undefined));
// {}
