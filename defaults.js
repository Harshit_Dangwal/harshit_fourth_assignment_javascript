function fillUndefinedProperties(objects, defaultProps) {
    if (objects) {
        for (let key in objects) {
            if (key in defaultProps){
                objects[key]= defaultProps[key]
            }

        } return objects;
    } else {
        return {}
    }
}
//console.log(fillUndefinedProperties({ name: 'Bruce Wayne', age: 36, location: 'Gotham' }, { location: 'New York' }))

module.exports=fillUndefinedProperties;