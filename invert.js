function getObjectCopy(objects) {
    if (objects) {
        const copy = {};
        for (let key in objects) {
            copy[objects[key]] = key;
        }
        return copy;
    } else {
        return {};
    }
}

// console.log(getObjectCopy({ name: 'Bruce Wayne', age: 36, location: 'Gotham' }));
module.exports = getObjectCopy;