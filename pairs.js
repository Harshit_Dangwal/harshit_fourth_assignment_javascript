function getKeyValuePairs(objects) {
    if (objects) {
        const result = [];
        for(key in objects){
            result.push([key,objects[key]]);
        }
        return result;
    } else {
        return [];
    }
}
//console.log(getKeyValuePairs({ name: 'Bruce Wayne', age: 36, location: 'Gotham' }));

module.exports=getKeyValuePairs;