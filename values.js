function displayObjectValue(object){
    if(displayObjectValue){
        const temp = object; 
        const result= [];
        for(key in temp){
            result.push(temp[key]);
        }
        return result;
    }else{
        return [];
    }
}
// console.log(displayObjectValue({ name: 'Bruce Wayne', age: 36, location: 'Gotham' }));

module.exports = displayObjectValue;